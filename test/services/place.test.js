const assert = require('assert');
const app = require('../../src/app');

describe('\'place\' service', () => {
  it('registered the service', () => {
    const service = app.service('place');

    assert.ok(service, 'Registered the service');
  });
});
