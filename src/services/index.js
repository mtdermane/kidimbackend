const users = require('./users/users.service.js');
const place = require('./place/place.service.js');
const menu = require('./menu/menu.service.js');
const orders = require('./orders/orders.service.js');
const category = require('./category/category.service.js');
const options = require('./options/options.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(place);
  app.configure(menu);
  app.configure(orders);
  app.configure(category);
  app.configure(options);
};
