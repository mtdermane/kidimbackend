const { Service } = require('feathers-mongoose');

exports.Orders = class Orders extends Service {
  create(data, params) {
    const { order, ...restData } = data;
    const newOrder = JSON.parse(order);
    const newData = {
      ...restData,
      order: newOrder,
      seen: false,
    };
    return super.create(newData, params);
  }
};
