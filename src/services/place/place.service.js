// Initializes the `place` service on path `/place`
const { Place } = require('./place.class');
const createModel = require('../../models/place.model');
const hooks = require('./place.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/place', new Place(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('place');

  service.hooks(hooks);
};
