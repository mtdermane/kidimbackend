const mongoose = require('mongoose');
const logger = require('./logger');
module.exports = function (app) {
  console.log(app.get('mongodb'));
  mongoose.connect(
    'mongodb+srv://admin:ypstmxiw6zAhHSMC@cluster0-j1ayc.gcp.mongodb.net/kidimbackend?retryWrites=true&w=majority',
    { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true  }
  ).catch(err => {
    logger.error(err);
    process.exit(1);
  });

  mongoose.Promise = global.Promise;

  app.set('mongooseClient', mongoose);
};
